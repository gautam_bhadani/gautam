import React,{useState} from 'react';

function Hide() {
    const [status,setStatus]=React.useState(true)
  return (
    <div>
      {
          status? <h1>Gautam World</h1>:"null"
      }
    
      
      <button onClick={()=>setStatus(false)}>Hide</button>
      <button onClick={()=>setStatus(true)}>Show</button>
      <button onClick={()=>setStatus(!status)}>toggle</button>
    </div>
  );
}

export default Hide;