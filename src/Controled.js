import React,{useState} from 'react'

function Controled()
{
    let[val,setVal]=useState("Rao")
    let[item,setItem]=useState("Gautam")
    console.warn("scc")
    return(
        <div>
            <h2>controled component</h2>
            <input type="text"  value={val} onChange={(e)=>setVal(e.target.value)}/>
            <h2>Value: {val}</h2>
            <input type="text"  value={item} onChange={(e)=>setItem(e.target.value)}/>
            <h2>Value: {item}</h2>
        </div>
    )
}
export default Controled;