import logo from './logo.svg';
import './App.css';
import GetValue from './GetValue';
import Hide from './Hide'
import Form from './Form'
import Condition from './Condition'
import Props from './Props'
import Hooks from './Hooks'
import Member from './Member'
import 'bootstrap/dist/css/bootstrap.min.css';
import Tabled from './Tabled'
import NestedList from './NestedList'
import Reuse from './Reuse'
import Parent from './Parent'
import Pure from './Pure';
import Counter from './Counter';
import Usememo from './Usememo';
import Reference from './Reference';
import Reffuntional from './Reffuntional'
import Baap from './Baap'
import Controled from './Controled'
import Uncontroled from './Uncontroled'
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Navbar from './Navbar'
import ReviewForm from './ReviewForm'
import * as Yup from 'yup';
import LastValue from './LastValue';
import Api from './Api';
import Todo from './Todo';
import Previcousparentprop from './Previcousparentprop';
import Appc from './ContextApi/Appc';

function App() {
  function getData()
  {
    alert("hello this is my React World")
  }
  
  return (
    <div className="App">
      {/* <GetValue /> */}
      {/* <Hide /> */}
      {/* <Form/> */}
      {/* <Condition /> */}
      {/* <Props data={getData}/>
      <Member data={getData} /> */}
      {/* <Hooks /> */}
      {/* <Tabled /> */}
      {/* <NestedList /> */}
      {/* <Reuse/> */}
      {/* <Parent /> */}
      {/* <Pure/> */}
      {/* <Counter/> */}
      {/* <Usememo /> */}
      {/* <Reference/> */}
      {/* <Reffuntional /> */}
      {/* <Baap /> */}
      {/* <Controled /> */}
      {/* <Uncontroled /> */}
      {/* <Navbar /> */}
      {/* <ReviewForm/> */}
      {/* <LastValue /> */}
      {/* <Api /> */}
      {/* <Previcousparentprop /> */}
      {/* <Appc /> */}
      <Todo/>


    </div>
  );
}

export default App;
