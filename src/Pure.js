import React,{PureComponent} from 'react'
import Counter from './Counter';
class Pure extends React.Component {
    constructor()
    {
        super();
        this.state={
            count:1
        }
    }
    render()
    {
        // console.warn("check Rerendering")
        return(
            <div className="pure">
                <Counter count={this.state.count}/>


                {/* <h1>Pure Component in class form{this.state.count}</h1> */}
                 <button 
                 onClick={()=>this.setState({count:this.state.count+1})}>
                
                    Update Count</button>
            </div>
        );
    }

}

export default Pure;