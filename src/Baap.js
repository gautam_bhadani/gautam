import React,{useRef} from 'react'
import Beta from './Beta';

function Baap(){
    let inputRef=useRef(null)
    function updateInput()
    {
        console.warn("abc")
        inputRef.current.value="1000"
        inputRef.current.style.color="red"
        inputRef.current.focus();
    }

    return(
        <div>
            <h2>Bapp component</h2>
            <Beta ref={inputRef}/>
            <button onClick={updateInput}>Update input</button>
        </div>
    )
}
export default Baap;