import react, { useState, useEffect } from "react";

function Hooks() {
  const [data, setData] = useState(10);
  const [count, setCount] = useState(100);
  useEffect(() => {
    console.warn("called with data state");
  }, [data]);
  return (
    <div>
      <h1>Hooks Component</h1>
      <p> use effect and use state difference</p>
      <h2>Count:{count}</h2>
      <h2>Data:{data}</h2>

      <button className="btn btn-success mr-2" onClick={() => setCount(count + 1)}>Update Count</button>
      <button className="btn btn-success" onClick={() => setData(data + 1)}>Update Count</button>
    </div>
  );
}

export default Hooks;
