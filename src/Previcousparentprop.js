import React,{useState} from 'react'
import PreviousPropsHooks from './PreviousPropsHooks'
function Previcousparentprop(){
    const [count,setCount]=React.useState(0)

    return(
        <div>
            <h1>
                parent component props
            </h1>
            <PreviousPropsHooks count={count} />
            <button onClick={()=>setCount(Math.floor(Math.random()*10))}>Update counter</button>
        </div>
    )
}

export default Previcousparentprop;