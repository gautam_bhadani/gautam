import React from 'react';
import { CommonContext } from './CommonContext'
function UpdateButton(){
    
        return(
            
                
                <CommonContext.Consumer >
                    {
                        ({updateColor})=>(
                            <div>
                        <button onClick={()=>updateColor('yellow')}
                        >Update Yellow Button</button>

                        <button onClick={()=>updateColor('blue')}
                        >Update Blue Button</button>

                        <button onClick={()=>updateColor('aqua')}
                        >Update aqua Button</button>
                            </div>
                         
                        )
                    }
                </CommonContext.Consumer >
                
                
            
        );
    }

export default UpdateButton;