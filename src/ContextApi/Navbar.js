import React from 'react'
import { CommonContext } from './CommonContext';

class Navbar extends React.Component
{
    render(){
        return(
           
                
                <CommonContext.Consumer >
                    {
                        ({color})=>(
                            <h1 style={{backgroundColor:color}}>Navbar Header page</h1>
                        )
                    }
                </CommonContext.Consumer >
                
                
            
        );
    }

    
}
export default Navbar;