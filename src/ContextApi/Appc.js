import React from 'react'
import { CommonContext } from './CommonContext';
import Footer from './Footer'
import Navbar from './Navbar'
import Main from './Main'
import UpdateButton from './UpdateButton';
 
class Appc extends React.Component
{
    constructor()
    {
        super()
        this.updateColor=(color)=>{
            this.setState({
                color:color
            })
        }
        this.state={
            color:"green",
            updateColor:this.updateColor
        }
        
     
       
    }
    render(){
        return(
            
                
                <CommonContext.Provider value={this.state}>
                    <Navbar/>
                    <h1>Complete and easy example for context API</h1>
                    <Main />
                     <UpdateButton/>
                    <Footer/>
                </CommonContext.Provider >
                
                
            
        );
    }

    
}
export default Appc;