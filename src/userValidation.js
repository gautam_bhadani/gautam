
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

const userSchema = yupResolver(
   Yup.object().shape({
      employeeName: Yup.string().required("Please enter your name"),
      organisationName:Yup.string().required("Please enter company name"),
      organisationAddress:Yup.string().required("Please enter company address"),
      websiteURL: Yup.string().url('Please enter a valid website URL').required("Please enter website URL"),
      businessEmail: Yup.string()
         .email('Please enter a valid email address')
         .required("Please enter your business email address"),
      contactNo:Yup.string()
         .matches(
            /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
               "Invalid phone number"
            )
         .min(10,"Please enter a valid Contact Number")
         .max(10,"Please enter a valid Contact Number")
         .required("Please enter your contact number"),
      designation:Yup.string().required("Please enter your designation"),
      checkedB:Yup.boolean().oneOf([true],"Please accept terms & conditions").required("Please accept terms & conditions"),
      
   }),
)


export { userSchema }



// import * as Yup from 'yup';


// const userSchema = Yup.object().shape({

//     employeeName: Yup.string().required("*Name is a required field."),
//     organisationName:Yup.string().required("*Organisation is a required field."),
//     websiteUrl: Yup.string().url('Please enter a valid website URL.').required("*Website URL is a required field."),
//     businessEmail: Yup.string()
//        .email('Please enter a valid email address.')
//        .required("*Business Email is a required field"),
//     contactNo:Yup.string()
//        .matches(
//           /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
//              "Invalid phone number"
//           )
//        .min(10,"*Please enter a valid Contact Number.")
//        .max(10,"*Please enter a valid Contact Number.")
//        .required("*Contact Number is a required."),
//     designation:Yup.string().required("*Designation is a required field"),
//     checkedB:Yup.boolean().oneOf([true], "You must accept the terms and conditions"),
//     });

// export default userSchema


