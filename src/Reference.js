import { render } from "@testing-library/react";
import React,{createRef} from 'react';
class Reference extends React.Component {
    constructor(){
        super()
            this.inputRef=createRef();
    }
    componentDidMount(){
        // console.warn(this.inputRef.current.value="1000")

    }
    getVal()
    {
        console.warn(this.inputRef.current.value)
        this.inputRef.current.style.color="red"
        this.inputRef.current.style.backgroundColor="green"
    }

    render()
    {

        return (
            <div className="Reference">
                <h1>Reference class component</h1>
                <input type="text" ref={this.inputRef}/>
                <button onClick={()=>this.getVal()}>Check Ref</button>
            </div>
        )
    }
        
    
}

export default Reference;