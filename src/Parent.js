import React,{props} from 'react'
import Child from './Child';
import Props from './Props';

function Parent() {
    // let data="Gautam yadav"
    
    function parentAlert(data)
    {
        console.log(data)
        alert(data.name+ data.email);
    }
    return(
        
            <div className="new">
                <h1>Parent Component</h1>

                <Child alert={parentAlert} />
                
            </div>
        
    )
}

export default Parent;