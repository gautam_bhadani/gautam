// import React, { useState, useMemo } from "react";

// function Usememo() {
//   const [count, setCount] = useState(0);
//   const [item, setItem] = useState(10);

//   const multiCountMemo = useMemo(function multiCount() {
//     console.warn("Component re-render");
//     return count * 5
//   },[count]);
//   return (
//     <div className="mine">
//       <h1>Use Memo in functional component</h1>
//       <h1>{count}</h1>
//       <h2>{item}</h2>
//       <h3>{multiCountMemo}</h3>
//       <button className="mr-4 btn-danger" onClick={() => setCount(count + 1)}>
//         update count
//       </button>
//       <button className="mr-4 btn-danger" onClick={() => setItem(item * 5)}>
//         update item
//       </button>
//     </div>
//   );
// }

// export default Usememo;
