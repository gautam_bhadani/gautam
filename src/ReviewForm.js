import React, { useState } from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Sitelogo from "./know-my-employee-logo.png";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { FaRegWindowClose } from "react-icons/fa";
import Input from "@material-ui/core/Input";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { userSchema } from "./userValidation";
import * as Yup from "yup";
import Box from "@material-ui/core/Box";
import Lebal from "@material-ui/core/Box";
import Form from "@material-ui/core/Box";
import Portal from "@material-ui/core/Portal";
import { useForm } from "react-hook-form";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  formhead: {
    justifyContent: "center",
    marginTop: "2rem",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    justifyContent: "center",
  },
  heading: {
    color: "#008dd2",
    position: "relative",
  },
  icon: {
    color: "#000",
    position: "absolute",
    right: "0px",
  },
  sec: {
    width: "100%",
  },
  secinner: {
    width: "100%",
    minHeight: "80px",
  },
  bluetext: {
    color: "#008dd2",
    padding: "0px 4px",
  },
  agreeTerms: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    minHeight: "43px",
    flexWrap: "wrap",
  },
  lebaltext: {
    textAlign: "left",
    position: "relative",
  },
  error: {
    color: "red",
    position: "absolute",
    left: "0",
    top: "54px",
  },
  iconplus: {
    color: "red",
  }
}));

function ReviewForm() {
  const classes = useStyles();

  const [formData , setFormData] = useState({
    employeeName: ""
  })

  const [employeeName, setemployeeName] = useState("");
  const [organisationName, setorganisationName] = useState("");
  const [websiteUrl, setWebsiteUrl] = useState("");
  const [businessEmail, setbusinessEmail] = useState("");
  const [contactNo, setcontactNo] = useState("");
  const [designation, setdesignation] = useState("");
  const [checkedB, setcheckedB] = useState(false);
  const [organisationAddress, setorganisationAddress] = useState("");

  const [status, setStatus] = React.useState(false)

  const handleButtonClick = () => {
    // document.getElementById("myForm").reset();

    setemployeeName("");
    setorganisationName("");
    setWebsiteUrl("");
    setbusinessEmail("");
    setcontactNo("");
    setdesignation("");
    organisationAddress("")
    setcheckedB(false);
  };

  const handleSubmitted = () => {
    // event.preventDefault();
    console.log("name", employeeName);
    console.log("organisationName", organisationName);
    console.log("websiteUrl", websiteUrl);
    console.log("businessEmail", businessEmail);
    console.log("contactNo", contactNo);
    console.log("designation", designation);
    console.log("organisationAddress", organisationAddress);
    
    console.log(checkedB);
  };


  const {
    register,
    formState: { errors },
    handleSubmit
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
    resolver: userSchema
  });

  // const {
  //   register,
  //   handleSubmit,

  //   formState: { errors }
  // } = useForm({
  //   mode: 'onBlur',
  //   reValidateMode: 'onChange',
  //   resolver: userSchema
  // });
  console.log("errors", errors)
  return (
    <>
      <div className={classes.root}>
        <Grid container className={classes.formhead}>
          <Grid item sm={8} lg={6} xs={12}>
            <Paper className={classes.paper}>
              <Box variant="h2" component="h2" className={classes.heading}>
                Employeer Information
                <Box
                  varient="span"
                  component="span"
                  className={classes.icon}
                  onClick={handleButtonClick}
                >
                  <FaRegWindowClose />
                </Box>
              </Box>
              <Typography
                varient="form"
                component="form"
                id="myForm"
                className={classes.root}
                noValidate
                onSubmit={handleSubmit((data) => handleSubmitted(data))}

                autoComplete="off"
              // ref={(form) => (form = form)}
              >
                <Box className={classes.sec} clone>
                  <Lebal className={classes.lebaltext}>
                    Name
                    <TextField
                      id="standard-basic"
                      value={employeeName}
                      color="secondary"
                      className={classes.secinner}
                      placeholder="Name"
                      {...register('employeeName')}
                      onChange={(event) => {
                        setemployeeName(event.target.value);


                      }}
                      error={errors?.employeeName?.message}
                      helperText={errors?.employeeName?.message && errors.employeeName.message}
                    />

                  </Lebal>
                </Box>
                <Box className={classes.sec} clone>
                  <Lebal className={classes.lebaltext}>
                    Company Name <AddCircleOutlineIcon className={classes.iconplus} onClick={() => setStatus(!status)} />

                    <TextField
                      id="standard-new"
                      // label="Company Name"
                      color="secondary"
                      value={organisationName}
                      className={classes.secinner}
                      type="text"
                      placeholder="Company Name"
                      {...register('organisationName')}
                      onChange={(event) => {
                        setorganisationName(event.target.value);
                      }}
                      error={errors?.organisationName?.message}
                      helperText={errors?.organisationName?.message && errors.organisationName.message}
                    />
                    {
                      status ? 
                      <Typography>
                        <Box className={classes.sec} clone>
                          <Lebal className={classes.lebaltext}>
                            Company Address
                            <TextField
                              id="standard-basic"
                              value={organisationAddress}
                              color="secondary"
                              className={classes.secinner}
                              placeholder="Company Address"
                              {...register('organisationAddress')}
                              onChange={(event) => {
                                setorganisationAddress(event.target.value);


                              }}
                              error={errors?.organisationAddress?.message}
                              helperText={errors?.organisationAddress?.message && errors.organisationAddress.message}
                            />

                          </Lebal>
                        </Box>
                        <Box className={classes.sec} clone>
                          <Lebal className={classes.lebaltext}>
                            Company management
                            <TextField
                              id="standard-basic"
                              value={employeeName}
                              color="secondary"
                              className={classes.secinner}
                              placeholder="Company management"
                              {...register('employeeName')}
                              onChange={(event) => {
                                setemployeeName(event.target.value);


                              }}
                              error={errors?.employeeName?.message}
                              helperText={errors?.employeeName?.message && errors.employeeName.message}
                            />

                          </Lebal>
                        </Box>


                      </Typography> : ""
                    }
                    {/* <Typography
                      className={classes.error}
                      varient="span"
                      component="span"
                    >
                      Please enter a Company name
                    </Typography> */}
                  </Lebal>
                </Box>
                {/* {company} */}
                <Box className={classes.sec} clone>
                  <Lebal className={classes.lebaltext}>
                    Website Url
                    <TextField
                      id="standard-new"
                      // label="Website Url"
                      color="secondary"
                      type="url"
                      value={websiteUrl}
                      className={classes.secinner}
                      {...register('websiteUrl')}
                      onChange={(event) => {
                        setWebsiteUrl(event.target.value);
                      }}
                      placeholder="Website Url"
                      error={errors?.websiteURL?.message}
                      helperText={errors?.websiteURL?.message && errors.websiteURL.message}
                    />
                    {/* <Typography
                      className={classes.error}
                      varient="span"
                      component="span"
                    >
                      Please enter a Website url
                    </Typography> */}
                  </Lebal>
                </Box>
                <Box className={classes.sec} clone>
                  <Lebal className={classes.lebaltext}>
                    Bussiness Email
                    <TextField
                      id="standard-new"
                      // label="Bussiness Email"
                      color="secondary"
                      value={businessEmail}
                      type="businessEmail"
                      placeholder="Bussiness Email"
                      {...register('businessEmail')}
                      onChange={(event) => {
                        setbusinessEmail(event.target.value);
                      }}
                      className={classes.secinner}
                      error={errors?.businessEmail?.message}
                      helperText={errors?.businessEmail?.message && errors.businessEmail.message}
                    />
                    {/* <Typography
                      className={classes.error}
                      varient="span"
                      component="span"
                    >
                      Please enter a Bussiness Email
                    </Typography> */}
                  </Lebal>
                </Box>
                <Box className={classes.sec} clone>
                  <Lebal className={classes.lebaltext}>
                    Contact Number
                    <TextField
                      id="standard-new"
                      // label="Contact Number"
                      color="secondary"
                      type="number"
                      value={contactNo}
                      placeholder="Contact Number"
                      {...register('contactNo')}
                      onChange={(event) => {
                        setcontactNo(event.target.value);
                      }}
                      className={classes.secinner}
                      error={errors?.contactNo?.message}
                      helperText={errors?.contactNo?.message && errors.contactNo.message}
                    />
                    {/* <Typography
                      className={classes.error}
                      varient="span"
                      component="span"
                    >
                      Please enter a Contact No.
                    </Typography> */}
                  </Lebal>
                </Box>
                <Box className={classes.sec} clone>
                  <Lebal className={classes.lebaltext}>
                    Designation
                    <TextField
                      id="standard-new"
                      // label="Designation"
                      color="secondary"
                      type="text"
                      value={designation}
                      placeholder="Designation"
                      {...register('designation')}
                      onChange={(event) => {
                        setdesignation(event.target.value);
                      }}
                      className={classes.secinner}
                      error={errors?.designation?.message}
                      helperText={errors?.designation?.message && errors.designation.message}
                    />
                    {/* <Typography
                      className={classes.error}
                      varient="span"
                      component="span"
                    >
                      Please enter a Designation
                    </Typography> */}
                  </Lebal>
                  {/* <Box>plese enter name  </Box> */}
                </Box>
                <FormGroup aria-label="position" row>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    color="primary"
                    checked={checkedB}
                    // onChange={(event)=>{this.useState(Check.event.value)}}
                    {...register('checkedB')}
                    onChange={(event) => {
                      setcheckedB(event.target.checked);
                    }}
                    error={errors?.checkedB?.message}
                    helperText={errors?.checkedB?.message && errors.checkedB.message}
                  />
                  <Typography className={classes.agreeTerms}>
                    i agree to the
                    <span className={classes.bluetext}>
                      terms and conditions
                    </span>{" "}
                    and{" "}
                    <span className={classes.bluetext}> privacy policy </span>
                  </Typography>
                </FormGroup>
                <button
                  type="handleSubmit(event)"
                  color="primary"
                  className="btn btn-info btn-deign mt-3"
                  onClick={handleSubmitted}
                >
                  Continue
                </button>
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </>
  );
}

export default ReviewForm;
