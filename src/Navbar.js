import react,{useState} from 'react'
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';
import Sitelogo from "./know-my-employee-logo.png";
import CssBaseline from "@material-ui/core/CssBaseline";


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    logo:{
        maxWidth: '150px',
        height: '50px',
        display: 'flex',
        justifyContent: 'left',
    }
   
  }));

function Navbar() {
    const classes = useStyles();
  return (
      
    <>
        <Grid item xs={12}>
        <Paper className={classes.paper}>
          <img className={classes.logo} src={Sitelogo} />
        </Paper>
      </Grid>
      
    </>
      )
}

export default Navbar;