import React from "react";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

function Nested() {
  const user = [
    {
      name: "gautam",
      age: "26",
      email: "manali28@gmail.com",
      address: [
        { Hn: "11", city: "manali", country: "india" },
        { Hn: "12", city: "delhi", country: "india" },
        { Hn: "21", city: "panjab", country: "india" },
        { Hn: "15", city: "delhi", country: "india" },
      ],
    },
    {
      name: "anil",
      age: "27",
      email: "gautam.yadav28@gmail.com",
      address: [
        { Hn: "11", city: "manali", country: "india" },
        { Hn: "12", city: "delhi", country: "usa" },
        { Hn: "21", city: "panjab", country: "nepal" },
        { Hn: "51", city: "delhi", country: "india" },
      ],
    },
    {
      name: "rao",
      age: "22",
      email: "gautam.yadav@gmail.com",
      address: [
        { Hn: "11", city: "manali", country: "india" },
        { Hn: "12", city: "delhi", country: "usa" },
        { Hn: "21", city: "panjab", country: "Afganistan" },
        { Hn: "51", city: "delhi", country: "nepal" },
      ],
    },
    {
      name: "sam",
      age: "26",
      email: "sam.yadav@gmail.com",
      address: [
        { Hn: "11", city: "manali", county: "india" },
        { Hn: "12", city: "delhi", county: "usa" },
        { Hn: "21", city: "panjab", county: "Afganistan" },
        { Hn: "51", city: "delhi", country: "india" },
      ],
    },
  ];
  return (
    <div>
      <h1>Nested List</h1>
      <Table varient="dark" striped>
        <tbody>
          <tr>
          <td>Serial No.</td>
            <td>Name</td>
            <td>email</td>
            <td>Address</td>
          </tr>

          {user.map((item,i) => (
            <tr key={i}>
              <td>{i+1}</td>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>
                <Table varient="dark" striped>
                  <tbody>
                    {item.address.map((data) => (
                      <tr>
                        <td>{data.Hn}</td>
                        <td>{data.city}</td>
                        <td>{data.country}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}

export default Nested;
