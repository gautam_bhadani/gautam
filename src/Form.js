import react,{useState} from 'react'

function Form() {
    const [name,setName]=useState("");
    const [tnc,setTnc]=useState(false);
    const [interest,setInterest]=useState("")
    function getFormData(e){
        console.warn(name,tnc,interest)
        e.preventDefault()
    }
  return (
    <>
      <h1>Handle React Form </h1>
      <form onSubmit={getFormData}>
          <input type="text" placeholder="enter name" onChange={(e)=>setName(e.target.value)}/> <br/> <br/>
          <select onChange={(e)=>setInterest(e.target.value)}>
              <option>Salect option</option>
              <option>Marvel</option>
              <option>Dc</option>
          </select>
          <br/> <br/>
          <input type="checkbox" onChange={(e)=>setTnc(e.target.checked)}/><span>Accept terma and conditions</span><br/> <br/>
          <button type="submit">Submit</button>
      </form>
    </>
  );
}
export default Form;