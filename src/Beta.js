import React,{forwardRef} from 'react'

function Beta(props,ref)
{

    return(
        <div>
            <h2>Beta component</h2>
            <input type="text" ref={ref}/>
        </div>
    )
}
export default forwardRef(Beta);