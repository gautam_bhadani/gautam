import React from 'react'

function Child(props)
 {
     const data={name:'design king',email:'Gautamyadav28@gmail.com'}
     
    
    return(
        
            <div className="new">
                <h1>Child Component:</h1>
                <button onClick={()=>props.alert(data)} className="btn-dark btn"> Click me</button>
            </div>
    )
}

export default Child;